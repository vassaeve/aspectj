package ru.cbr.test.aspectj;

public final class Service2 {

    @Secured(isLocked = true)
    public String method1() {
        return "Service2:method1";
    }

    @Secured(isLocked = false)
    public String method2() {
        return "Service2:method2";
    }

}
