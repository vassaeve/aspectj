package ru.cbr.test.aspectj;

import org.aspectj.weaver.loadtime.WeavingURLClassLoader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;

public enum RuntimeWeaver {
    ;

    public static void main(String[] args)
            throws MalformedURLException, ClassNotFoundException, NoSuchMethodException,
            IllegalAccessException, InstantiationException, InvocationTargetException {

        ///inner plugin
        Service1 service1 = new Service1();
        System.out.println(service1.method1());
        System.out.println(service1.method2());


        ///outer plugin
        URL urlOfJar = Paths.get("C:\\projects\\TEST\\pboaspectj\\plugin2\\target\\plugin2-1.0.jar")
                .toUri()
                .toURL();
        URLClassLoader classLoader = new URLClassLoader(
                new URL[]{urlOfJar},
                ClassLoader.getSystemClassLoader()
        );
        WeavingURLClassLoader weaver = new WeavingURLClassLoader(classLoader);
        Class<?> service2 = weaver.loadClass("ru.cbr.test.aspectj.Service2");

        ///Пример вызова.
        ///!!!!! call pointcut does not pick out reflective calls to a method implemented in java.lang.reflect.Method.invoke(Object, Object[]).
        Object instance = service2.newInstance();
        Method method1 = service2.getMethod("method1");
        Method method2 = service2.getMethod("method2");

        System.out.println(method1.invoke(instance));

        System.out.println(method2.invoke(instance));
    }

}
