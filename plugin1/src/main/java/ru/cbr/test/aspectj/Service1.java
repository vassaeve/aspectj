package ru.cbr.test.aspectj;

public final class Service1 {

    @Secured(isLocked = true)
    public String method1() {
        return "Service1:method1";
    }

    @Secured(isLocked = false)
    public String method2() {
        return "Service1:method2";
    }

}
