package ru.cbr.test.aspectj;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class SecuredMethodAspect {

    @Around("callAt(secured) ")
    public Object around(ProceedingJoinPoint pjp, Secured secured) throws Throwable {
        System.out.println(pjp.getSignature().getName() + "() target method call....");
        return secured.isLocked() ? "access denied(shared)" : pjp.proceed();
    }

//    @Pointcut("execution(@ru.cbr.test.aspectj.Secured public * *(..))")
//    public void annotated() {}

    @Pointcut("@annotation(secured)")
    public void callAt(Secured secured) {
    }

}